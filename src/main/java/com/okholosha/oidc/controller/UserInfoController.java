package com.okholosha.oidc.controller;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jwt.JWTClaimsSet;
import com.okholosha.oidc.controller.constants.OpenIdEndpoints;
import com.okholosha.oidc.domain.dto.UserInfo;
import com.okholosha.oidc.domain.entity.JsonWebToken;
import com.okholosha.oidc.domain.entity.UserEntity;
import com.okholosha.oidc.service.UserService;
import com.okholosha.oidc.service.TokenService;
import com.okholosha.oidc.service.JsonWebTokenService;
import com.okholosha.oidc.util.AuthenticationUtil;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.util.Optional;
import java.util.UUID;

@CrossOrigin(originPatterns = "*", allowCredentials = "true", allowedHeaders = "*")
@RestController
@RequestMapping(OpenIdEndpoints.USER_INFO_ENDPOINT)
@AllArgsConstructor
public class UserInfoController {

    private final TokenService tokenService;
    private final UserService userService;
    private final JsonWebTokenService jsonWebTokenService;

    @GetMapping
    public ResponseEntity<UserInfo> userInfo(@RequestHeader("Authorization") String authorizationHeader) {
        String tokenValue = AuthenticationUtil.fromBearerAuthHeader(authorizationHeader);
        JsonWebToken jsonWebToken = tokenService.findJsonWebToken(tokenValue);
        Optional<UserEntity> user;
        if (jsonWebToken == null) {
            return getUnauthorizedErrorResponseEntity("Access Token is invalid");
        }
        try {
            JWTClaimsSet jwtClaimsSet = jsonWebTokenService.parseAndValidateToken(jsonWebToken.getValue());
            user = userService.findUserByIdentifier(UUID.fromString(jwtClaimsSet.getSubject()));
            return user.map(u -> ResponseEntity.ok(new UserInfo(u))).orElse(
                    ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                            .header("WWW-Authenticate", "Bearer")
                            .build());
        } catch (ParseException | JOSEException e) {
            return getUnauthorizedErrorResponseEntity("Access Token is invalid");
        }
    }

    private ResponseEntity<UserInfo> getUnauthorizedErrorResponseEntity(String message) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                .header("WWW-Authenticate", "Bearer")
                .body(new UserInfo("invalid_token", message));
    }

    @ExceptionHandler(MissingRequestHeaderException.class)
    public ResponseEntity<UserInfo> handle(MissingRequestHeaderException ex) {
        return getUnauthorizedErrorResponseEntity("Access Token is required");
    }
}
