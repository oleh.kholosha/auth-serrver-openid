package com.okholosha.oidc.domain.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.security.authentication.BadCredentialsException;

import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@DiscriminatorValue("opaque")
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = true)
public class OpaqueToken extends Token {

  @NotBlank
  @Size(max = 200)
  private String subject;

  @NotBlank
  @Size(max = 200)
  private String clientId;

  @NotBlank
  @Size(max = 200)
  private String issuer;

  @NotNull
  @ElementCollection(fetch = FetchType.EAGER)
  private Set<String> scope;

  @NotNull
  private LocalDateTime issuedAt;

  @NotNull
  private LocalDateTime notBefore;

  @NotNull
  private boolean refreshToken;

  public boolean isRefreshToken() {
    return refreshToken;
  }

  public void validate() {
    if (LocalDateTime.now().isAfter(this.getExpiry())) {
      throw new BadCredentialsException("Expired");
    }
    if (LocalDateTime.now().isBefore(this.getNotBefore())) {
      throw new BadCredentialsException("Not yet valid");
    }
  }

}
