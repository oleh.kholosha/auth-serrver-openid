package com.okholosha.oidc.domain.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class UserResource extends UserListResource {

    private String email;

    private Set<String> roles = new HashSet<>();

    public UserResource(UUID identifier, String externalId, String userName, String familyName,
                        String givenName, String userType, String locale, boolean active,
                        String email, Set<String> roles) {
        super(identifier, externalId, userName, familyName, givenName, userType, locale, active);
        this.email = email;
        this.roles = roles;
    }

}
