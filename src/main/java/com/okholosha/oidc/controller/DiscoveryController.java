package com.okholosha.oidc.controller;

import com.okholosha.oidc.controller.constants.OpenIdEndpoints;
import com.okholosha.oidc.domain.dto.Scope;
import com.okholosha.oidc.domain.dto.Discovery;
import com.okholosha.oidc.domain.dto.GrantType;
import com.okholosha.oidc.service.JwtPki;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(originPatterns = "*", allowCredentials = "true", allowedHeaders = "*")
@RestController
@RequestMapping(OpenIdEndpoints.DISCOVERY_ENDPOINT)
@AllArgsConstructor
public class DiscoveryController {

  private final JwtPki jwtPki;

  @GetMapping
  public Discovery discoveryEndpoint() {
    Discovery discovery = new Discovery();
    discovery.setAuthorization_endpoint(jwtPki.getIssuer() + OpenIdEndpoints.AUTHORIZATION_ENDPOINT);
    discovery.setIssuer(jwtPki.getIssuer());
    discovery.setToken_endpoint(jwtPki.getIssuer() + OpenIdEndpoints.TOKEN_ENDPOINT);
    discovery.setIntrospection_endpoint(jwtPki.getIssuer() + OpenIdEndpoints.INTROSPECTION_ENDPOINT);
    discovery.setRevocation_endpoint(jwtPki.getIssuer() + OpenIdEndpoints.REVOCATION_ENDPOINT);
    discovery.setUserinfo_endpoint(jwtPki.getIssuer() + OpenIdEndpoints.USER_INFO_ENDPOINT);
    discovery.setJwks_uri(jwtPki.getIssuer() + "/jwks");
    discovery.getGrant_types_supported().add(GrantType.AUTHORIZATION_CODE.getGrant());
    discovery.getResponse_types_supported().add("code");
    discovery.getScopes_supported().add(Scope.OPENID.name().toLowerCase());
    discovery.getScopes_supported().add(Scope.OFFLINE_ACCESS.name().toLowerCase());
    discovery.getScopes_supported().add(Scope.PROFILE.name().toLowerCase());
    discovery.getScopes_supported().add(Scope.EMAIL.name().toLowerCase());
    discovery.getResponse_modes_supported().add("query");
    discovery.getResponse_modes_supported().add("form_post");
    discovery.getSubject_types_supported().add("public");
    discovery.getId_token_signing_alg_values_supported().add("RS256");
    discovery.getToken_endpoint_auth_methods_supported().add("client_secret_basic");
    discovery.getToken_endpoint_auth_methods_supported().add("client_secret_post");
    discovery.getClaims_supported().add("aud");
    discovery.getClaims_supported().add("auth_time");
    discovery.getClaims_supported().add("created_at");
    discovery.getClaims_supported().add("gender");
    discovery.getClaims_supported().add("birthdate");
    discovery.getClaims_supported().add("locale");
    discovery.getClaims_supported().add("zoneinfo");
    discovery.getClaims_supported().add("email");
    discovery.getClaims_supported().add("email_verified");
    discovery.getClaims_supported().add("exp");
    discovery.getClaims_supported().add("website");
    discovery.getClaims_supported().add("family_name");
    discovery.getClaims_supported().add("given_name");
    discovery.getClaims_supported().add("iat");
    discovery.getClaims_supported().add("identities");
    discovery.getClaims_supported().add("iss");
    discovery.getClaims_supported().add("identities");
    discovery.getClaims_supported().add("name");
    discovery.getClaims_supported().add("nickname");
    discovery.getClaims_supported().add("sub");
    discovery.getToken_endpoint_auth_signing_alg_values_supported().add("RS256");
    return discovery;
  }
}
