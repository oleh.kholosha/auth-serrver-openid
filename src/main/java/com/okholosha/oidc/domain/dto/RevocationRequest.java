package com.okholosha.oidc.domain.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RevocationRequest {

    private String token;
    private String token_type_hint;

}
