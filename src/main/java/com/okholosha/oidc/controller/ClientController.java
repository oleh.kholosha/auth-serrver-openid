package com.okholosha.oidc.controller;

import com.okholosha.oidc.domain.dto.ModifyRegisteredClientResource;
import com.okholosha.oidc.domain.dto.RegisteredClientResource;
import com.okholosha.oidc.domain.entity.RegisteredClient;
import com.okholosha.oidc.service.RegisteredClientService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/clients")
@AllArgsConstructor
public class ClientController {

  private final RegisteredClientService registeredClientService;

  @GetMapping
  public List<RegisteredClientResource> clients() {
    return registeredClientService.findAll().stream()
        .map(RegisteredClientResource::new)
        .collect(Collectors.toList());
  }

  @GetMapping("/{clientId}")
  public ResponseEntity<RegisteredClientResource> client(@PathVariable("clientId") UUID clientId) {
    return registeredClientService.findOneByIdentifier(clientId)
            .map(c -> ResponseEntity.ok(new RegisteredClientResource(c)))
            .orElse(ResponseEntity.notFound().build());
  }

  @PostMapping
  public ResponseEntity<RegisteredClientResource> registerNewClient(
          @RequestBody @Valid ModifyRegisteredClientResource modifyRegisteredClientResource) {
    RegisteredClient registeredClient = new RegisteredClient(modifyRegisteredClientResource);
    registeredClient = this.registeredClientService.create(registeredClient);
    return ResponseEntity.ok().body(new RegisteredClientResource(registeredClient));
  }

  @PutMapping("/{clientId}")
  public ResponseEntity<RegisteredClientResource> update(@PathVariable("clientId") UUID clientId,
                                             @Valid @RequestBody ModifyRegisteredClientResource modifyRegisteredClientResource) {
    return registeredClientService.update(clientId, new RegisteredClient(modifyRegisteredClientResource))
            .map(RegisteredClientResource::new)
            .map(ResponseEntity::ok)
            .orElse(ResponseEntity.notFound().build());
  }

  @DeleteMapping("/{clientId}")
  public ResponseEntity<Void> deleteClient(@PathVariable("clientId") UUID clientId) {
    registeredClientService.deleteOneByIdentifier(clientId);
    return ResponseEntity.noContent().build();
  }

}
