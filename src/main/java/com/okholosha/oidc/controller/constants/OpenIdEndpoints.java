package com.okholosha.oidc.controller.constants;

public final class OpenIdEndpoints {

    private OpenIdEndpoints() {
    }

    public static final String AUTHORIZATION_ENDPOINT = "/authorize";
    public static final String INTROSPECTION_ENDPOINT = "/introspect";
    public static final String REVOCATION_ENDPOINT = "/revoke";
    public static final String DISCOVERY_ENDPOINT = "/.well-known/openid-configuration";
    public static final String USER_INFO_ENDPOINT = "/userinfo";
    public static final String TOKEN_ENDPOINT = "/token";

}
