package com.okholosha.oidc.domain.dto;

import com.okholosha.oidc.domain.entity.RegisteredClient;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.ElementCollection;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class RegisteredClientResource {

  private UUID identifier;
  @NotBlank
  @Size(max = 100)
  private String clientId;
  @NotNull
  @Enumerated(EnumType.STRING)
  private AccessTokenFormat accessTokenFormat;
  @NotEmpty
  @ElementCollection
  private Set<GrantType> grantTypes = new HashSet<>();
  @NotEmpty
  private Set<String> redirectUris = new HashSet<>();
  @NotEmpty
  private Set<String> corsUris = new HashSet<>();

  public RegisteredClientResource(RegisteredClient registeredClient) {
    this.identifier = registeredClient.getIdentifier();
    this.accessTokenFormat = registeredClient.getAccessTokenFormat();
    this.clientId = registeredClient.getClientId();
    this.corsUris = registeredClient.getCorsUris();
    this.grantTypes = registeredClient.getGrantTypes();
    this.redirectUris = registeredClient.getRedirectUris();
  }

}
