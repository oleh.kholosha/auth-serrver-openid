package com.okholosha.oidc.repository;

import com.okholosha.oidc.domain.entity.Token;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TokenRepository<T extends Token> extends JpaRepository<T, Long> {}
