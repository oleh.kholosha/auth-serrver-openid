package com.okholosha.oidc.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ClientCredentials {

  private final String clientId;
  private final String clientSecret;

}
