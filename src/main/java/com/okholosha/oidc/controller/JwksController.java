package com.okholosha.oidc.controller;

import com.okholosha.oidc.service.JwtPki;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@CrossOrigin(originPatterns = "*", allowCredentials = "true", allowedHeaders = "*")
@RestController
@RequestMapping("/jwks")
@AllArgsConstructor
public class JwksController {

  private final JwtPki jwtPki;

  @GetMapping
  public Map<String, Object> jwksEndpoint() {
    return jwtPki.getJwkSet().toJSONObject();
  }
}
