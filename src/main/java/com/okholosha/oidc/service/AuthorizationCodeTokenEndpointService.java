package com.okholosha.oidc.service;

import com.okholosha.oidc.config.AuthorizationServerConfigurationProperties;
import com.okholosha.oidc.domain.dto.AccessTokenFormat;
import com.okholosha.oidc.domain.dto.AuthorizationCode;
import com.okholosha.oidc.domain.dto.ClientCredentials;
import com.okholosha.oidc.domain.dto.GrantType;
import com.okholosha.oidc.domain.dto.Scope;
import com.okholosha.oidc.domain.dto.TokenRequest;
import com.okholosha.oidc.domain.dto.TokenResponse;
import com.okholosha.oidc.domain.entity.RegisteredClient;
import com.okholosha.oidc.domain.entity.UserEntity;
import com.okholosha.oidc.util.TokenEndpointHelper;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.okholosha.oidc.domain.dto.TokenResponse.BEARER_TOKEN_TYPE;

@Service
@AllArgsConstructor
public class AuthorizationCodeTokenEndpointService {

    private final RegisteredClientService registeredClientService;
    private final AuthorizationCodeService authorizationCodeService;
    private final TokenService tokenService;
    private final UserService userService;
    private final AuthorizationServerConfigurationProperties authorizationServerProperties;
    private final RegisteredClientAuthenticationService registeredClientAuthenticationService;

    public ResponseEntity<TokenResponse> getTokenResponseForAuthorizationCode(
            String authorizationHeader, TokenRequest tokenRequest) {
        ClientCredentials clientCredentials = TokenEndpointHelper.retrieveClientCredentials(authorizationHeader, tokenRequest);
        if (clientCredentials == null) {
            return TokenEndpointHelper.reportInvalidClientError();
        }
        AuthorizationCode authorizationCode = authorizationCodeService.getCode(tokenRequest.getCode());
        if (authorizationCode == null) {
            return TokenEndpointHelper.reportInvalidClientError();
        }
        if (authorizationCode.isExpired()) {
            authorizationCodeService.removeCode(authorizationCode.getCode());
            return TokenEndpointHelper.reportInvalidClientError();
        }
        if (!clientCredentials.getClientId().equals(authorizationCode.getClientId())) {
            return TokenEndpointHelper.reportInvalidClientError();
        }
        return registeredClientService.findOneByClientId(clientCredentials.getClientId())
                .map(registeredClient -> {
                    if (!registeredClient.getGrantTypes().contains(GrantType.AUTHORIZATION_CODE)) {
                        return TokenEndpointHelper.reportInvalidGrantError();
                    }
                    try {
                        registeredClientAuthenticationService.authenticate(
                                clientCredentials.getClientId(), clientCredentials.getClientSecret());
                    } catch (AuthenticationException ex) {
                        return TokenEndpointHelper.reportInvalidClientError();
                    }
                    return userService.findUserByIdentifier(UUID.fromString(authorizationCode.getSubject())).map(user -> {
                                return createTokenResponse(authorizationCode, registeredClient, user);
                            })
                            .orElse(TokenEndpointHelper.reportInvalidGrantError());
                })
                .orElse(TokenEndpointHelper.reportInvalidGrantError());
    }

    private ResponseEntity<TokenResponse> createTokenResponse(AuthorizationCode authorizationCode,
                                                              RegisteredClient registeredClient, UserEntity user) {
        Duration accessTokenLifetime = authorizationServerProperties.getAccessToken().getLifetime();
        Duration refreshTokenLifetime = authorizationServerProperties.getRefreshToken().getLifetime();
        Duration idTokenLifetime = authorizationServerProperties.getIdToken().getLifetime();
        authorizationCodeService.removeCode(authorizationCode.getCode());
        return ResponseEntity.ok(createTokenResponse(authorizationCode, registeredClient, user, accessTokenLifetime,
                        refreshTokenLifetime, idTokenLifetime));
    }

    private TokenResponse createTokenResponse(AuthorizationCode authorizationCode, RegisteredClient registeredClient,
                                              UserEntity user, Duration accessTokenLifetime, Duration refreshTokenLifetime, Duration idTokenLifetime) {
        return new TokenResponse(
                createAccessToken(authorizationCode, registeredClient, user, accessTokenLifetime),
                createRefreshToken(authorizationCode, user, refreshTokenLifetime),
                accessTokenLifetime.toSeconds(),
                createIdToken(authorizationCode, user, idTokenLifetime),
                BEARER_TOKEN_TYPE);
    }

    private String createIdToken(AuthorizationCode authorizationCode, UserEntity user, Duration idTokenLifetime) {
        return authorizationCode.getScopes().stream()
                .map(String::toUpperCase)
                .collect(Collectors.toList())
                .contains(Scope.OPENID.name())
                ? tokenService.createIdToken(user, authorizationCode.getClientId(), authorizationCode.getScopes(),
                        idTokenLifetime).getValue() : null;
    }

    private String createRefreshToken(AuthorizationCode authorizationCode, UserEntity user,
                                      Duration refreshTokenLifetime) {
        return tokenService.createPersonalizedRefreshToken(authorizationCode.getClientId(),
                        user, authorizationCode.getScopes(), refreshTokenLifetime).getValue();
    }

    private String createAccessToken(AuthorizationCode authorizationCode, RegisteredClient registeredClient,
                                     UserEntity user, Duration accessTokenLifetime) {
        return AccessTokenFormat.JWT.equals(registeredClient.getAccessTokenFormat())
                ? tokenService.createPersonalizedJwtAccessToken(user, authorizationCode.getClientId(),
                        authorizationCode.getScopes(), accessTokenLifetime)
                .getValue() : tokenService.createPersonalizedOpaqueAccessToken(user, authorizationCode.getClientId(),
                        authorizationCode.getScopes(), accessTokenLifetime).getValue();
    }
}
