package com.okholosha.oidc.domain.dto;

import lombok.Getter;
import lombok.ToString;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.Set;

@ToString
@Getter
public class AuthorizationCode {

  private final String clientId;
  private final URI redirectUri;
  private final Set<String> scopes;
  private final String code;
  private final LocalDateTime expiry;
  private final String subject;

  public AuthorizationCode(
          String clientId,
          URI redirectUri,
          Set<String> scopes,
          String code,
          String subject) {
    this.clientId = clientId;
    this.redirectUri = redirectUri;
    this.scopes = scopes;
    this.code = code;
    this.subject = subject;
    this.expiry = LocalDateTime.now().plusMinutes(2);
  }

  public boolean isExpired() {
    return LocalDateTime.now().isAfter(getExpiry());
  }

}
