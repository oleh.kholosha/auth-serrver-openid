package com.okholosha.oidc.controller;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jwt.JWTClaimsSet;
import com.okholosha.oidc.controller.constants.OpenIdEndpoints;
import com.okholosha.oidc.domain.dto.AuthorizationCode;
import com.okholosha.oidc.domain.dto.GrantType;
import com.okholosha.oidc.domain.dto.IntrospectionRequest;
import com.okholosha.oidc.domain.dto.IntrospectionResponse;
import com.okholosha.oidc.domain.dto.RevocationRequest;
import com.okholosha.oidc.domain.dto.RevocationResponse;
import com.okholosha.oidc.domain.dto.TokenRequest;
import com.okholosha.oidc.domain.dto.TokenResponse;
import com.okholosha.oidc.domain.entity.EndUserDetails;
import com.okholosha.oidc.domain.entity.JsonWebToken;
import com.okholosha.oidc.domain.entity.RegisteredClient;
import com.okholosha.oidc.domain.entity.UserEntity;
import com.okholosha.oidc.service.AuthorizationCodeService;
import com.okholosha.oidc.service.AuthorizationCodeTokenEndpointService;
import com.okholosha.oidc.service.RefreshTokenEndpointService;
import com.okholosha.oidc.service.RegisteredClientService;
import com.okholosha.oidc.service.UserService;
import com.okholosha.oidc.service.TokenService;
import com.okholosha.oidc.service.JsonWebTokenService;
import com.okholosha.oidc.util.AuthenticationUtil;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.constraints.Pattern;
import java.io.IOException;
import java.net.URI;
import java.text.ParseException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Validated
@CrossOrigin(originPatterns = "*", allowCredentials = "true", allowedHeaders = "*")
@Controller
@AllArgsConstructor
public class AuthorizationController {

    private final AuthorizationCodeService authorizationCodeService;
    private final RegisteredClientService registeredClientService;
    private final TokenService tokenService;
    private final UserService userService;
    private final JsonWebTokenService jsonWebTokenService;
    private final RefreshTokenEndpointService refreshTokenEndpointService;
    private final AuthorizationCodeTokenEndpointService authorizationCodeTokenEndpointService;

    @SuppressWarnings({"unused", "SpringMVCViewInspection"})
    @PreAuthorize("isAuthenticated()")
    @GetMapping(OpenIdEndpoints.AUTHORIZATION_ENDPOINT)
    public String authorizationRequest(
            @RequestParam("response_type") @Pattern(regexp = "code") String responseType,
            @RequestParam("scope") String scope,
            @RequestParam("client_id") String clientId,
            @RequestParam("redirect_uri") URI redirectUri,
            @RequestParam(name = "state", required = false) String state,
            @AuthenticationPrincipal EndUserDetails endUserDetails) {
        if (endUserDetails == null || endUserDetails.getIdentifier() == null) {
            throw new BadCredentialsException("No user");
        }
        Optional<RegisteredClient> registeredClient =
                registeredClientService.findOneByClientId(clientId);
        if (registeredClient.isEmpty()) {
            throw new InvalidClientIdError(clientId);
        }
        RegisteredClient client = registeredClient.get();
        if (!client.getRedirectUris().contains(redirectUri.toString())) {
            throw new InvalidRedirectUriError(redirectUri.toString());
        }
        if (!client.getGrantTypes().contains(GrantType.AUTHORIZATION_CODE)) {
            return redirectError(redirectUri, "unauthorized_client",
                    "The client is not authorized to request an authorization code using this method",
                    state);
        }
        Set<String> scopes = new HashSet<>(Arrays.asList(scope.split(" ")));
        AuthorizationCode authorizationCode = authorizationCodeService.createAndStoreAuthorizationState(
                clientId, redirectUri, scopes,
                endUserDetails.getIdentifier() != null ? endUserDetails.getIdentifier().toString() : "");

        return "redirect:" + redirectUri + "?code=" + authorizationCode.getCode() + "&state=" + state;
    }

    @ResponseBody
    @PostMapping(OpenIdEndpoints.TOKEN_ENDPOINT)
    public ResponseEntity<TokenResponse> getToken(
            @RequestHeader(name = "Authorization", required = false) String authorizationHeader,
            @ModelAttribute("token_request") TokenRequest tokenRequest) {
        if (tokenRequest.getGrant_type().equalsIgnoreCase(GrantType.AUTHORIZATION_CODE.getGrant())) {
            return authorizationCodeTokenEndpointService.getTokenResponseForAuthorizationCode(authorizationHeader, tokenRequest);
        } else if (tokenRequest.getGrant_type().equalsIgnoreCase(GrantType.REFRESH_TOKEN.getGrant())) {
            return refreshTokenEndpointService.getTokenResponseForRefreshToken(authorizationHeader, tokenRequest);
        } else {
            return ResponseEntity.badRequest().body(new TokenResponse("unsupported_grant_type"));
        }
    }

    @ResponseBody
    @PostMapping(OpenIdEndpoints.INTROSPECTION_ENDPOINT)
    public ResponseEntity<IntrospectionResponse> introspect(
            @RequestHeader("Authorization") String authorizationHeader,
            @ModelAttribute("introspection_request") IntrospectionRequest introspectionRequest) {
        try {
            if (AuthenticationUtil.fromBasicAuthHeader(authorizationHeader) == null) {
                return reportInvalidClientError();
            }
            String tokenValue = introspectionRequest.getToken();
            if (tokenValue == null || tokenValue.isBlank()) {
                return ResponseEntity.ok(new IntrospectionResponse(false));
            }
            JsonWebToken jsonWebToken = tokenService.findJsonWebToken(tokenValue);
            if (jsonWebToken == null) {
                return ResponseEntity.ok(new IntrospectionResponse(false));
            }
            return ResponseEntity.ok(getIntrospectionResponse(jsonWebToken));
        } catch (BadCredentialsException ex) {
            return reportInvalidClientError();
        }
    }

    @ResponseBody
    @PostMapping(OpenIdEndpoints.REVOCATION_ENDPOINT)
    public ResponseEntity<RevocationResponse> revoke(
            @ModelAttribute("revocation_request") RevocationRequest revocationRequest) {
        JsonWebToken jsonWebToken = tokenService.findJsonWebAccessToken(revocationRequest.getToken());
        if (jsonWebToken == null) {
            return ResponseEntity.badRequest().body(new RevocationResponse(null, "invalid_request"));
        }
        tokenService.remove(jsonWebToken);
        return ResponseEntity.ok(new RevocationResponse("ok", null));
    }

    private IntrospectionResponse getIntrospectionResponse(JsonWebToken jsonWebToken) {
        try {
            JWTClaimsSet jwtClaimsSet =
                    jsonWebTokenService.parseAndValidateToken(jsonWebToken.getValue());
            String clientId = jwtClaimsSet.getStringClaim("client_id");
            String subject = jwtClaimsSet.getSubject();
            Optional<UserEntity> user = userService.findUserByIdentifier(UUID.fromString(subject));
            return user.map(
                            u -> createIntrospectionResult(jwtClaimsSet, clientId, u))
                    .orElse(new IntrospectionResponse(false));
        } catch (ParseException | JOSEException e) {
            return new IntrospectionResponse(false);
        }
    }

    private IntrospectionResponse createIntrospectionResult(JWTClaimsSet jwtClaimsSet, String clientId, UserEntity u) {
        IntrospectionResponse introspectionResponse = new IntrospectionResponse();
        introspectionResponse.setActive(true);
        introspectionResponse.setClient_id(clientId);
        introspectionResponse.setSub(u.getIdentifier().toString());
        introspectionResponse.setUsername(u.getUserName());
        introspectionResponse.setIss(jwtClaimsSet.getIssuer());
        introspectionResponse.setNbf(jwtClaimsSet.getNotBeforeTime().getTime());
        introspectionResponse.setIat(jwtClaimsSet.getIssueTime().getTime());
        introspectionResponse.setExp(jwtClaimsSet.getExpirationTime().getTime());
        return introspectionResponse;
    }

    private ResponseEntity<IntrospectionResponse> reportInvalidClientError() {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                .header("WWW-Authenticate", "Basic")
                .body(new IntrospectionResponse("invalid_client"));
    }

    private String redirectError(URI redirectUri, String errorCode, String errorDescription, String state) {
        return "redirect:" + redirectUri.toString() + "?error=" + errorCode + "&error_description=" + errorDescription
                + "&state=" + state;
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<String> handle(MissingServletRequestParameterException ex, HttpServletResponse httpServletResponse)
            throws IOException {
        httpServletResponse.sendError(400, "invalid_request");
        return ResponseEntity.badRequest()
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .body("error=invalid_request");
    }

    @ExceptionHandler(InvalidClientIdError.class)
    public ResponseEntity<String> handle(InvalidClientIdError ex, HttpServletResponse httpServletResponse) throws IOException {
        httpServletResponse.sendError(400, "invalid client");
        return ResponseEntity.badRequest()
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .body("error=invalid client");
    }

    @ExceptionHandler(InvalidRedirectUriError.class)
    public ResponseEntity<String> handle(InvalidRedirectUriError ex, HttpServletResponse httpServletResponse) throws IOException {
        httpServletResponse.sendError(400, "redirect uri mismatch");
        return ResponseEntity.badRequest()
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .body("error=redirect uri mismatch");
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<String> handle(ConstraintViolationException ex, HttpServletResponse httpServletResponse) throws IOException {
        if (!ex.getConstraintViolations().isEmpty()) {
            ConstraintViolation<?> constraintViolation = ex.getConstraintViolations().iterator().next();
            if (constraintViolation.getPropertyPath().toString().contains("responseType")) {
                httpServletResponse.sendError(400, "unsupported_response_type");
                return ResponseEntity.badRequest()
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .body("error=unsupported_response_type");
            } else {
                httpServletResponse.sendError(400, "invalid_request");
                return ResponseEntity.badRequest()
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .body("error=invalid_request");
            }
        } else {
            httpServletResponse.sendError(400, "server_error");
            return ResponseEntity.badRequest()
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                    .body("error=server_error");
        }
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<String> handle(MissingServletRequestParameterException ex) {
        return ResponseEntity.badRequest().body(ex.getMessage());
    }

    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<String> handle(BadCredentialsException ex) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(ex.getMessage());
    }

    @ExceptionHandler(JOSEException.class)
    public ResponseEntity<String> handle(JOSEException ex) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
    }

    static class InvalidClientIdError extends RuntimeException {
        InvalidClientIdError(String clientId) {
            super("Invalid client id " + clientId);
        }
    }

    static class InvalidRedirectUriError extends RuntimeException {
        InvalidRedirectUriError(String redirectUri) {
            super("Invalid redirect URI " + redirectUri);
        }
    }

}
