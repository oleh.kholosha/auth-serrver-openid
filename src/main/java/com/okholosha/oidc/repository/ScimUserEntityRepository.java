package com.okholosha.oidc.repository;

import com.okholosha.oidc.domain.entity.UserEntity;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface ScimUserEntityRepository extends JpaRepository<UserEntity, Long> {

    @EntityGraph(attributePaths = {"email", "roles"})
    Optional<UserEntity> findOneByIdentifier(UUID identifier);

    @EntityGraph(attributePaths = {"email", "roles"})
    Optional<UserEntity> findOneByUserName(String username);

    void deleteOneByIdentifier(UUID identifier);

}
