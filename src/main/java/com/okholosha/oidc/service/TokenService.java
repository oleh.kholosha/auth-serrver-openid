package com.okholosha.oidc.service;

import com.okholosha.oidc.config.AuthorizationServerConfigurationProperties;
import com.okholosha.oidc.domain.entity.UserEntity;
import com.okholosha.oidc.repository.JsonWebTokenRepository;
import com.okholosha.oidc.repository.OpaqueTokenRepository;
import com.okholosha.oidc.domain.entity.JsonWebToken;
import com.okholosha.oidc.domain.entity.OpaqueToken;
import com.nimbusds.jose.JOSEException;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Set;

@Service
@Transactional(readOnly = true)
@AllArgsConstructor
public class TokenService {

    private static final Logger LOG = LoggerFactory.getLogger(TokenService.class);

    private final AuthorizationServerConfigurationProperties authorizationServerProperties;
    private final JsonWebTokenRepository jsonWebTokenRepository;
    private final OpaqueTokenRepository opaqueTokenRepository;
    private final JsonWebTokenService jsonWebTokenService;
    private final OpaqueTokenService opaqueTokenService;

    public JsonWebToken findJsonWebToken(String value) {
        return jsonWebTokenRepository.findOneByValue(value);
    }

    public JsonWebToken findJsonWebAccessToken(String value) {
        return jsonWebTokenRepository.findOneByValueAndAccessToken(value, true);
    }

    public OpaqueToken findOpaqueToken(String value) {
        return opaqueTokenRepository.findOneByValue(value);
    }

    @Transactional
    public JsonWebToken createIdToken(UserEntity user, String clientId, Set<String> scopes, Duration idTokenLifetime) {
        try {
            LocalDateTime expiryDateTime = LocalDateTime.now().plusMinutes(idTokenLifetime.toMinutes());
            String token = jsonWebTokenService.createPersonalizedToken(false, clientId,
                    Collections.singletonList(clientId), scopes, user, expiryDateTime);
            JsonWebToken jsonWebToken = new JsonWebToken();
            jsonWebToken.setExpiry(expiryDateTime);
            jsonWebToken.setValue(token);
            jsonWebToken.setAccessToken(false);
            return jsonWebTokenRepository.save(jsonWebToken);
        } catch (JOSEException e) {
            LOG.error("Error creating Id token", e);
            throw new TokenServiceException("Error creating Id token", e);
        }
    }

    @Transactional
    public JsonWebToken createPersonalizedJwtAccessToken(
            UserEntity user, String clientId, Set<String> scopes, Duration accessTokenLifetime) {
        LocalDateTime expiryDateTime = LocalDateTime.now().plusMinutes(accessTokenLifetime.toMinutes());
        try {
            String token = jsonWebTokenService.createPersonalizedToken(true, clientId,
                    Collections.singletonList(clientId), scopes, user, expiryDateTime);
            JsonWebToken jsonWebToken = new JsonWebToken();
            jsonWebToken.setExpiry(expiryDateTime);
            jsonWebToken.setValue(token);
            jsonWebToken.setAccessToken(true);
            return jsonWebTokenRepository.save(jsonWebToken);
        } catch (JOSEException e) {
            LOG.error("Error creating a personalized JWT", e);
            throw new TokenServiceException("Error creating a personalized JWT", e);
        }
    }

    @Transactional
    public OpaqueToken createPersonalizedOpaqueAccessToken(
            UserEntity user, String clientId, Set<String> scopes, Duration accessTokenLifetime) {
        LocalDateTime issueTime = LocalDateTime.now();
        LocalDateTime expiryDateTime = issueTime.plusMinutes(accessTokenLifetime.toMinutes());
        String token = opaqueTokenService.createToken();
        OpaqueToken opaqueToken = new OpaqueToken();
        opaqueToken.setRefreshToken(false);
        opaqueToken.setExpiry(expiryDateTime);
        opaqueToken.setValue(token);
        opaqueToken.setClientId(clientId);
        opaqueToken.setIssuedAt(issueTime);
        opaqueToken.setNotBefore(issueTime);
        opaqueToken.setIssuer(authorizationServerProperties.getIssuer().toString());
        opaqueToken.setScope(scopes);
        opaqueToken.setSubject(user.getIdentifier().toString());
        return opaqueTokenRepository.save(opaqueToken);
    }

    @Transactional
    public OpaqueToken createPersonalizedRefreshToken(
            String clientId, UserEntity user, Set<String> scopes, Duration refreshTokenLifetime) {
        LocalDateTime issueTime = LocalDateTime.now();
        LocalDateTime expiryDateTime = issueTime.plusMinutes(refreshTokenLifetime.toMinutes());
        String token = opaqueTokenService.createToken();
        OpaqueToken opaqueToken = new OpaqueToken();
        opaqueToken.setRefreshToken(true);
        opaqueToken.setExpiry(expiryDateTime);
        opaqueToken.setIssuedAt(issueTime);
        opaqueToken.setNotBefore(issueTime);
        opaqueToken.setIssuer(authorizationServerProperties.getIssuer().toString());
        opaqueToken.setScope(scopes);
        opaqueToken.setValue(token);
        opaqueToken.setRefreshToken(true);
        opaqueToken.setClientId(clientId);
        opaqueToken.setSubject(user.getIdentifier().toString());
        return opaqueTokenRepository.save(opaqueToken);
    }

    @Transactional
    public void remove(OpaqueToken entity) {
        opaqueTokenRepository.delete(entity);
    }

    @Transactional
    public void remove(JsonWebToken entity) {
        jsonWebTokenRepository.delete(entity);
    }
}
