package com.okholosha.oidc.repository;

import com.okholosha.oidc.domain.entity.JsonWebToken;

public interface JsonWebTokenRepository extends TokenRepository<JsonWebToken> {

  JsonWebToken findOneByValue(String value);

  JsonWebToken findOneByValueAndAccessToken(String value, boolean accessToken);
}
