package com.okholosha.oidc.service;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JOSEObjectType;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import com.okholosha.oidc.domain.dto.Scope;
import com.okholosha.oidc.domain.entity.UserEntity;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.IdGenerator;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class JsonWebTokenService {

    private static final JOSEObjectType JWT_TYP_ACCESS_TOKEN = new JOSEObjectType("at+jwt");

    private final JwtPki jwtPki;
    private final IdGenerator idGenerator;

    public JWTClaimsSet parseAndValidateToken(String token) throws ParseException, JOSEException {
        SignedJWT signedJWT = SignedJWT.parse(token);
        signedJWT.verify(jwtPki.getVerifier());
        return signedJWT.getJWTClaimsSet();
    }

    public String createPersonalizedToken(boolean isAccessToken, String clientId, List<String> audiences,
                                          Set<String> scopes, UserEntity user, LocalDateTime expiryDateTime) throws JOSEException {
        JWTClaimsSet.Builder builder = new JWTClaimsSet.Builder()
                .subject(user.getIdentifier().toString())
                .issuer(jwtPki.getIssuer())
                .expirationTime(Date.from(expiryDateTime.atZone(ZoneId.systemDefault()).toInstant()))
                .audience(audiences)
                .issueTime(new Date())
                .notBeforeTime(new Date())
                .jwtID(idGenerator.generateId().toString())
                .claim("name", user.getUserName())
                .claim("client_id", clientId)
                .claim("locale", "de");

        if (user.getRoles() != null && !user.getRoles().isEmpty()) {
            builder.claim("roles", user.getRoles());
        }
        if (!scopes.isEmpty()) {
            Set<String> scopesToCompare = scopes.stream().map(String::toUpperCase).collect(Collectors.toSet());
            builder.claim("scope", String.join(" ", scopes));
            if (scopesToCompare.contains(Scope.PROFILE.name())) {
                builder.claim("nickname", user.getUserName())
                        .claim("preferred_username", user.getUserName())
                        .claim("family_name", user.getFamilyName())
                        .claim("given_name", user.getGivenName());
            }
            if (scopesToCompare.contains(Scope.EMAIL.name())) {
                builder.claim("email", user.getEmail())
                        .claim("email_verified", Boolean.TRUE);
            }
        }
        JWTClaimsSet claimsSet = builder.build();
        SignedJWT signedJWT = createSignedJWT(isAccessToken, claimsSet);
        signedJWT.sign(jwtPki.getSigner());
        return signedJWT.serialize();
    }

    private SignedJWT createSignedJWT(boolean isAccessToken, JWTClaimsSet claimsSet) {
        JWSHeader.Builder jwsHeaderBuilder = new JWSHeader.Builder(JWSAlgorithm.RS256);
        jwsHeaderBuilder.keyID(jwtPki.getPublicKey().getKeyID());
        if (isAccessToken) {
            jwsHeaderBuilder.type(JWT_TYP_ACCESS_TOKEN);
        }
        return new SignedJWT(jwsHeaderBuilder.build(), claimsSet);
    }
}
