package com.okholosha.oidc.domain.dto;

public enum Scope {
  OPENID,
  OFFLINE_ACCESS,
  EMAIL,
  PROFILE
}
