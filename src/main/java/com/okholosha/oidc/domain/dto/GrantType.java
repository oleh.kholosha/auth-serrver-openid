package com.okholosha.oidc.domain.dto;

public enum GrantType {
  AUTHORIZATION_CODE("authorization_code"),
  REFRESH_TOKEN("refresh_token");

  private final String grant;

  GrantType(String grant) {
    this.grant = grant;
  }

  public String getGrant() {
    return grant;
  }
}
