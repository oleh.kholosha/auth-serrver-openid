package com.okholosha.oidc.domain.dto;

public enum AccessTokenFormat {
  OPAQUE,
  JWT
}
