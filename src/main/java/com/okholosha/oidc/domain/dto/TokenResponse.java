package com.okholosha.oidc.domain.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TokenResponse {

  public static final String BEARER_TOKEN_TYPE = "Bearer";

  private String access_token;
  private String token_type;
  private String issued_token_type;
  private String refresh_token;
  private String scope;
  private long expires_in;
  private String id_token;
  private String error;

  public TokenResponse(
          String access_token,
          String refresh_token,
          long expires_in,
          String id_token,
          String token_type) {
    this(access_token, refresh_token, expires_in, id_token, token_type, null, null);
  }

  @JsonCreator
  public TokenResponse(
          String access_token,
          String refresh_token,
          long expires_in,
          String id_token,
          String token_type,
          String issued_token_type,
          String scope) {
    this.access_token = access_token;
    this.refresh_token = refresh_token;
    this.expires_in = expires_in;
    this.id_token = id_token;
    this.token_type = token_type;
    this.issued_token_type = issued_token_type;
    this.scope = scope;
  }

  public TokenResponse(String error) {
    this.error = error;
  }

}
