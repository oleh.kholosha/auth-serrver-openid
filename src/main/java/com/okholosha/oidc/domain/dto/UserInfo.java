package com.okholosha.oidc.domain.dto;

import com.okholosha.oidc.domain.entity.UserEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class UserInfo {

    private String sub;
    private String name;
    private String given_name;
    private String family_name;
    private String middle_name;
    private String nickname;
    private String preferred_username;
    private String website;
    private String profile;
    private String picture;
    private String email;
    private String gender;
    private String birthdate;
    private String zoneinfo;
    private String locale;
    private Set<String> roles;
    private String updated_at;
    private String error;
    private String error_description;

    public UserInfo(String subject) {
        this.sub = subject;
        this.name = subject;
    }

    public UserInfo(String error, String error_description) {
        this.error = error;
        this.error_description = error_description;
    }

    public UserInfo(UserEntity user) {
        this.email = user.getEmail();
        this.given_name = user.getGivenName();
        this.family_name = user.getFamilyName();
        this.preferred_username = user.getUserName();
        this.name = user.getUserName();
        this.sub = user.getIdentifier().toString();
        this.locale = user.getLocale();
        this.roles = user.getRoles();
    }

}
