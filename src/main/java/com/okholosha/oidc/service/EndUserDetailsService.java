package com.okholosha.oidc.service;

import com.okholosha.oidc.domain.entity.EndUserDetails;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Qualifier("endUserDetailsService")
@Service
@AllArgsConstructor
public class EndUserDetailsService implements UserDetailsService {

    private final UserService userService;

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return this.userService.findUserByUserName(username)
                .map(EndUserDetails::new)
                .orElseThrow(() -> new UsernameNotFoundException("No user found"));
    }
}
