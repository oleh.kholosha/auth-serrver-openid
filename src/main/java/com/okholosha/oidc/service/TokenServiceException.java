package com.okholosha.oidc.service;

public class TokenServiceException extends RuntimeException {

  public TokenServiceException(String message) {
    super(message);
  }

  public TokenServiceException(String message, Throwable cause) {
    super(message, cause);
  }
}
