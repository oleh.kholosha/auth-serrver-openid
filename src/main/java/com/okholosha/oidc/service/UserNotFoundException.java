package com.okholosha.oidc.service;

import java.util.UUID;

public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException(UUID userIdentifier) {
        super(String.format("No user found with identifier %s", userIdentifier));
    }
}
