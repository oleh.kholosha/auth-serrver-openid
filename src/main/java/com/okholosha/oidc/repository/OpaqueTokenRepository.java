package com.okholosha.oidc.repository;

import com.okholosha.oidc.domain.entity.OpaqueToken;

public interface OpaqueTokenRepository extends TokenRepository<OpaqueToken> {

  OpaqueToken findOneByValue(String value);

}
