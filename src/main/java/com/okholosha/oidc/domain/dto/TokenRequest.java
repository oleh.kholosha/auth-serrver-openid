package com.okholosha.oidc.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.net.URI;

@Getter
@Setter
@AllArgsConstructor
public class TokenRequest {

  @NotBlank
  private final String grant_type;
  private final String code;
  private final URI redirect_uri;
  private final String client_id;
  private final String client_secret;
  private final String code_verifier;
  private final String username;
  private final String password;
  private final String refresh_token;
  private final String subject_token;
  private final String subject_token_type;
  private final String scope;
  private final String resource;
  private final String audience;
  private final String requested_token_type;
  private final String actor_token;
  private final String actor_token_type;

}
