package com.okholosha.oidc.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.net.URI;
import java.time.Duration;

@Getter
@Setter
@ConfigurationProperties(prefix = "auth-server")
public class AuthorizationServerConfigurationProperties {

  private URI issuer;
  private AccessToken accessToken;
  private IdToken idToken;
  private RefreshToken refreshToken;

  @Getter
  @Setter
  public abstract static class Token {
    private Duration lifetime;

  }

  public enum TokenType {
    JWT,
    OPAQUE
  }

  @Getter
  @Setter
  public static class AccessToken extends Token {
    private TokenType defaultFormat;
  }

  public static class IdToken extends Token {}

  @Getter
  @Setter
  public static class RefreshToken extends Token {
    private Duration maxLifetime;
  }
}
