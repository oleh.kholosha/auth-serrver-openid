package com.okholosha.oidc.domain.entity;

import com.okholosha.oidc.domain.dto.ModifyRegisteredClientResource;
import com.okholosha.oidc.domain.dto.AccessTokenFormat;
import com.okholosha.oidc.domain.dto.GrantType;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.net.URI;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
@Getter
@Setter
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class RegisteredClient extends AbstractPersistable<Long> {

  private UUID identifier;
  @NotBlank
  @Size(max = 100)
  @Column(unique = true)
  private String clientId;
  @Size(max = 100)
  private String clientSecret;
  @NotNull
  @Enumerated(EnumType.STRING)
  private AccessTokenFormat accessTokenFormat;
  @NotEmpty
  @ElementCollection(fetch = FetchType.EAGER)
  private Set<GrantType> grantTypes = new HashSet<>();
  @NotEmpty
  @ElementCollection(fetch = FetchType.EAGER)
  private Set<String> redirectUris = new HashSet<>();
  @NotEmpty
  @ElementCollection(fetch = FetchType.EAGER)
  private Set<String> corsUris = new HashSet<>();

  public RegisteredClient(ModifyRegisteredClientResource modifyRegisteredClientResource) {
    this(UUID.randomUUID(), modifyRegisteredClientResource.getClientId(),
            modifyRegisteredClientResource.getClientSecret(),
            modifyRegisteredClientResource.getAccessTokenFormat(), modifyRegisteredClientResource.getGrantTypes(),
            modifyRegisteredClientResource.getRedirectUris(), modifyRegisteredClientResource.getCorsUris());
  }

}
