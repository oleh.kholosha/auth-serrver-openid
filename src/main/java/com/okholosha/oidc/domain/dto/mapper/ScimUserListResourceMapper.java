package com.okholosha.oidc.domain.dto.mapper;

import com.okholosha.oidc.domain.dto.UserListResource;
import com.okholosha.oidc.domain.entity.UserEntity;
import org.springframework.stereotype.Component;

@Component
public class ScimUserListResourceMapper {

    public UserListResource mapEntityToResource(UserEntity scimUserEntity) {
        return new UserListResource(
                scimUserEntity.getIdentifier(), scimUserEntity.getExternalId(), scimUserEntity.getUserName(),
                scimUserEntity.getFamilyName(), scimUserEntity.getGivenName(), scimUserEntity.getUserType(),
                 scimUserEntity.getLocale(), scimUserEntity.isActive());
    }
}
