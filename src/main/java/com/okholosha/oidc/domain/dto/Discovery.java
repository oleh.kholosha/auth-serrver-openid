package com.okholosha.oidc.domain.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Discovery {

  private String issuer;
  private String authorization_endpoint;
  private String token_endpoint;
  private String userinfo_endpoint;
  private String jwks_uri;
  private String registration_endpoint;
  private String introspection_endpoint;
  private String revocation_endpoint;
  private String device_authorization_endpoint;
  private String request_object_endpoint;
  private String pushed_authorization_request_endpoint;
  private List<String> scopes_supported = new ArrayList<>();
  private List<String> response_types_supported = new ArrayList<>();
  private List<String> response_modes_supported = new ArrayList<>();
  private List<String> grant_types_supported = new ArrayList<>();
  private List<String> acr_values_supported = new ArrayList<>();
  private List<String> subject_types_supported = new ArrayList<>();
  private List<String> id_token_signing_alg_values_supported = new ArrayList<>();
  private List<String> token_endpoint_auth_methods_supported = new ArrayList<>();
  private List<String> token_endpoint_auth_signing_alg_values_supported = new ArrayList<>();
  private List<String> claims_supported = new ArrayList<>();

}
