package com.okholosha.oidc.domain.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class UserListResource extends Resource {

    public static final String SCIM_USER_SCHEMA = "urn:ietf:params:scim:schemas:core:2.0:User";
    public static final String SCIM_ENTERPRISE_USER_SCHEMA = "urn:ietf:params:scim:schemas:extension:enterprise:2.0:User";

    @NotNull
    @NotBlank
    @Size(min = 3, max = 50)
    private String userName;
    @NotNull
    @NotBlank
    @Size(min = 1, max = 100)
    private String familyName;
    @NotNull
    @NotBlank
    @Size(min = 1, max = 100)
    private String givenName;
    @Size(max = 100)
    private String userType;
    @Size(max = 50)
    private String locale;
    @NotNull
    private boolean active;

    public UserListResource(UUID identifier, String externalId, String userName, String familyName,
                            String givenName, String userType,
                            String locale, boolean active) {
        super(List.of(SCIM_USER_SCHEMA), identifier, externalId);
        this.userName = userName;
        this.familyName = familyName;
        this.givenName = givenName;
        this.userType = userType;
        this.locale = locale;
        this.active = active;
    }

}
