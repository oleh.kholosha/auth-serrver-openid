package com.okholosha.oidc;

import com.okholosha.oidc.domain.dto.AccessTokenFormat;
import com.okholosha.oidc.domain.dto.GrantType;
import com.okholosha.oidc.domain.entity.RegisteredClient;
import com.okholosha.oidc.domain.entity.UserEntity;
import com.okholosha.oidc.repository.RegisteredClientRepository;
import com.okholosha.oidc.repository.ScimUserEntityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Set;
import java.util.UUID;

@Component
public class DataInitializer implements CommandLineRunner {

    private static final Logger LOG = LoggerFactory.getLogger(DataInitializer.class);

    private static final UUID ADMIN_ID = UUID.fromString("55bc7a01-3db2-46cd-95f9-c4f8d4ae9557");
    private static final UUID USER_ID = UUID.fromString("39fd95ec-4f6f-47f7-92f0-16bbb7f832ee");

    private final ScimUserEntityRepository scimUserEntityRepository;
    private final RegisteredClientRepository registeredClientRepository;

    private final PasswordEncoder passwordEncoder;

    public DataInitializer(
            ScimUserEntityRepository scimUserEntityRepository,
            RegisteredClientRepository registeredClientRepository,
            PasswordEncoder passwordEncoder) {
        this.scimUserEntityRepository = scimUserEntityRepository;
        this.registeredClientRepository = registeredClientRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Transactional
    @Override
    public void run(String... args) {
        long countUsers = this.scimUserEntityRepository.count();
        if (countUsers == 0) {
            createAdminUser();
            createUsers();
        }
        long countClients = this.registeredClientRepository.count();
        if (countClients == 0) {
            createClients();
        }
    }

    private void createAdminUser() {
        UserEntity adminUser = new UserEntity(
                ADMIN_ID,
                "admin",
                "AdminSurname",
                "AdminName",
                true,
                passwordEncoder.encode("admin"),
                "admin@gmail.com",
                Set.of("USER", "ADMIN"));

        adminUser = scimUserEntityRepository.save(adminUser);

        LOG.info("Created {} SCIM admin user", adminUser);
    }

    private void createUsers() {
        scimUserEntityRepository.save(new UserEntity(
                USER_ID,
                "user",
                "UserSurname",
                "UserName",
                true,
                passwordEncoder.encode("password"),
                "test_user@example.com",
                Collections.singleton("USER")));
    }

    private void createClients() {
        registeredClientRepository.save(new RegisteredClient(
                UUID.randomUUID(),
                "client-id",
                passwordEncoder.encode("client-secret"),
                AccessTokenFormat.JWT,
                Set.of(GrantType.AUTHORIZATION_CODE, GrantType.REFRESH_TOKEN),
                Collections.singleton(
                        "http://localhost:8080/demo-client/login/oauth2/code/demo"),
                Collections.singleton("*")));
    }
}
