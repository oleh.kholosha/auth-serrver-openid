package com.okholosha.oidc.domain.entity;

import lombok.EqualsAndHashCode;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@Entity
@DiscriminatorValue("jwt")
@Setter
@ToString
@EqualsAndHashCode(callSuper = true)
public class JsonWebToken extends Token {

  @NotNull
  private boolean accessToken;

  public boolean isAccessToken() {
    return accessToken;
  }

  public boolean isIdToken() {
    return !accessToken;
  }

}
