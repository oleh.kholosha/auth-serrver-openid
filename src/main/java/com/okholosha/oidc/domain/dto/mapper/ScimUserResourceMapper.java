package com.okholosha.oidc.domain.dto.mapper;

import com.okholosha.oidc.domain.dto.UserResource;
import com.okholosha.oidc.domain.entity.UserEntity;
import org.springframework.stereotype.Component;

import static java.util.Collections.emptySet;

@Component
public class ScimUserResourceMapper {

    public UserResource mapEntityToResource(UserEntity scimUserEntity) {
        return new UserResource(
                scimUserEntity.getIdentifier(), scimUserEntity.getExternalId(), scimUserEntity.getUserName(),
                scimUserEntity.getFamilyName(), scimUserEntity.getGivenName(), scimUserEntity.getUserType(),
                scimUserEntity.getLocale(), scimUserEntity.isActive(), scimUserEntity.getEmail(),
                scimUserEntity.getRoles() != null ? scimUserEntity.getRoles() : emptySet());
    }
}
