package com.okholosha.oidc.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class Resource implements Serializable {

    @NotNull
    @NotEmpty
    private List<String> schemas = new ArrayList<>();
    private UUID identifier;
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 50)
    private String externalId;

}
