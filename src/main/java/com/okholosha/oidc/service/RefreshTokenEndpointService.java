package com.okholosha.oidc.service;

import com.okholosha.oidc.config.AuthorizationServerConfigurationProperties;
import com.okholosha.oidc.domain.dto.AccessTokenFormat;
import com.okholosha.oidc.domain.dto.ClientCredentials;
import com.okholosha.oidc.domain.dto.GrantType;
import com.okholosha.oidc.domain.dto.TokenRequest;
import com.okholosha.oidc.domain.dto.TokenResponse;
import com.okholosha.oidc.domain.entity.OpaqueToken;
import com.okholosha.oidc.domain.entity.RegisteredClient;
import com.okholosha.oidc.domain.entity.UserEntity;
import com.okholosha.oidc.util.TokenEndpointHelper;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static com.okholosha.oidc.domain.dto.TokenResponse.BEARER_TOKEN_TYPE;

@Service
@AllArgsConstructor
public class RefreshTokenEndpointService {

    private final TokenService tokenService;
    private final UserService userService;
    private final AuthorizationServerConfigurationProperties authorizationServerProperties;
    private final RegisteredClientAuthenticationService registeredClientAuthenticationService;

    public ResponseEntity<TokenResponse> getTokenResponseForRefreshToken(String authorizationHeader,
                                                                         TokenRequest tokenRequest) {
        ClientCredentials clientCredentials =
                TokenEndpointHelper.retrieveClientCredentials(authorizationHeader, tokenRequest);
        if (clientCredentials == null) {
            return TokenEndpointHelper.reportInvalidClientError();
        }
        Duration accessTokenLifetime = authorizationServerProperties.getAccessToken().getLifetime();
        Duration refreshTokenLifetime = authorizationServerProperties.getRefreshToken().getLifetime();
        RegisteredClient registeredClient;
        try {
            registeredClient = registeredClientAuthenticationService.authenticate(
                            clientCredentials.getClientId(), clientCredentials.getClientSecret());

        } catch (AuthenticationException ex) {
            return TokenEndpointHelper.reportInvalidClientError();
        }
        if (registeredClient.getGrantTypes().contains(GrantType.REFRESH_TOKEN)) {
            OpaqueToken opaqueWebToken = tokenService.findOpaqueToken(tokenRequest.getRefresh_token());
            if (opaqueWebToken != null && opaqueWebToken.isRefreshToken()) {
                opaqueWebToken.validate();
                Set<String> scopes = new HashSet<>();
                if (StringUtils.isNotBlank(tokenRequest.getScope())) {
                    scopes = new HashSet<>(Arrays.asList(tokenRequest.getScope().split(" ")));
                }
                Optional<UserEntity> authenticatedUser = userService.findUserByIdentifier(
                        UUID.fromString(opaqueWebToken.getSubject()));
                if (authenticatedUser.isPresent()) {
                    return ResponseEntity.ok(new TokenResponse(
                            getAccessToken(clientCredentials, accessTokenLifetime, registeredClient, scopes,
                                    authenticatedUser.get()),
                                    tokenService.createPersonalizedRefreshToken(clientCredentials.getClientId(),
                                                    authenticatedUser.get(), scopes, refreshTokenLifetime).getValue(),
                                    accessTokenLifetime.toSeconds(), null, BEARER_TOKEN_TYPE));
                }
            }
            tokenService.remove(opaqueWebToken);
            return TokenEndpointHelper.reportInvalidClientError();
        } else {
            return TokenEndpointHelper.reportUnauthorizedClientError();
        }
    }

    private String getAccessToken(ClientCredentials clientCredentials, Duration accessTokenLifetime,
                                  RegisteredClient registeredClient, Set<String> scopes,
                                  UserEntity authenticatedUser) {
        return AccessTokenFormat.JWT.equals(registeredClient.getAccessTokenFormat())
                ? tokenService.createPersonalizedJwtAccessToken(authenticatedUser, clientCredentials.getClientId(),
                scopes, accessTokenLifetime).getValue()
                : tokenService.createPersonalizedOpaqueAccessToken(authenticatedUser, clientCredentials.getClientId(),
                scopes, accessTokenLifetime).getValue();
    }
}
