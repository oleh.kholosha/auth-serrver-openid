package com.okholosha.oidc.domain.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Size;

@Getter
@Setter
@ToString(callSuper = true)
@NoArgsConstructor
public class ModifyRegisteredClientResource extends RegisteredClientResource {

  @Size(max = 100)
  private String clientSecret;

}