package com.okholosha.oidc.domain.dto.mapper;

import com.okholosha.oidc.domain.dto.CreateUserResource;
import com.okholosha.oidc.domain.entity.UserEntity;
import org.springframework.stereotype.Component;

@Component
public class CreateScimUserResourceMapper {

    public UserEntity mapResourceToEntity(CreateUserResource createScimUserResource) {
        return new UserEntity(createScimUserResource.getIdentifier(),
                createScimUserResource.getExternalId(), createScimUserResource.getUserName(), createScimUserResource.getFamilyName(),
                createScimUserResource.getGivenName(), createScimUserResource.getUserType(), createScimUserResource.getLocale(),
                createScimUserResource.isActive(), createScimUserResource.getPassword(), createScimUserResource.getEmail(),
                createScimUserResource.getRoles()
        );
    }
}
