package com.okholosha.oidc.controller;

import com.okholosha.oidc.domain.dto.CreateUserResource;
import com.okholosha.oidc.domain.dto.UserListResource;
import com.okholosha.oidc.domain.dto.UserResource;
import com.okholosha.oidc.domain.dto.mapper.CreateScimUserResourceMapper;
import com.okholosha.oidc.domain.dto.mapper.ScimUserListResourceMapper;
import com.okholosha.oidc.domain.dto.mapper.ScimUserResourceMapper;
import com.okholosha.oidc.domain.entity.EndUserDetails;
import com.okholosha.oidc.domain.entity.UserEntity;
import com.okholosha.oidc.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.NO_CONTENT;

@Validated
@RestController
@AllArgsConstructor
public class UserController {

    public static final String USER_ENDPOINT = "/api/users";
    public static final String ME_ENDPOINT = "/api/me";

    private final UserService userService;
    private final ScimUserResourceMapper scimUserResourceMapper;
    private final ScimUserListResourceMapper scimUserListResourceMapper;
    private final CreateScimUserResourceMapper createScimUserResourceMapper;

    @PostMapping(USER_ENDPOINT)
    public ResponseEntity<UserResource> createUser(@Valid @RequestBody CreateUserResource createScimUserResource) {
        UserEntity scimUserEntity = createScimUserResourceMapper.mapResourceToEntity(createScimUserResource);
        scimUserEntity = userService.createUser(scimUserEntity);
        return ResponseEntity.ok().body(scimUserResourceMapper.mapEntityToResource(scimUserEntity));
    }

    @GetMapping(USER_ENDPOINT)
    public List<UserListResource> getAllUsers() {
        return userService.findAllUsers().stream()
                .map(scimUserListResourceMapper::mapEntityToResource)
                .collect(Collectors.toList());
    }

    @GetMapping(USER_ENDPOINT + "/{userId}")
    public ResponseEntity<UserResource> getUser(@PathVariable("userId") UUID userIdentifier) {
        return userService.findUserByIdentifier(userIdentifier).map(ue ->
                ResponseEntity.ok().body(scimUserResourceMapper.mapEntityToResource(ue)))
                .orElse(ResponseEntity.notFound().build());
    }

    @PutMapping(USER_ENDPOINT + "/{userId}")
    public ResponseEntity<UserResource> updateUser(@PathVariable("userId") UUID userIdentifier,
                                                   @RequestBody @Valid UserResource scimUserResource) {
        UserEntity ue = userService.updateUser(userIdentifier, scimUserResource);
        return ResponseEntity.ok().body(scimUserResourceMapper.mapEntityToResource(ue));
    }

    @ResponseStatus(NO_CONTENT)
    @DeleteMapping(USER_ENDPOINT + "/{userId}")
    public void deleteUser(@PathVariable("userId") UUID userIdentifier) {
        userService.deleteUser(userIdentifier);
    }


    @GetMapping(ME_ENDPOINT)
    public ResponseEntity<UserResource> getAuthenticatedUser(@AuthenticationPrincipal EndUserDetails user) {
        return userService.findUserByIdentifier(user.getIdentifier()).map(ue ->
                ResponseEntity.ok().body(scimUserResourceMapper.mapEntityToResource(ue)))
                .orElse(ResponseEntity.notFound().build());
    }
}
