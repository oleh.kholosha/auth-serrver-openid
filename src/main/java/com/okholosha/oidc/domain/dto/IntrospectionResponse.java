package com.okholosha.oidc.domain.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class IntrospectionResponse {

  private boolean active;
  private String scope;
  private String client_id;
  private String username;
  private String token_type;
  private long exp;
  private long iat;
  private long nbf;
  private String sub;
  private List<String> aud;
  private String iss;
  private String jti;
  private String error;

  public IntrospectionResponse(boolean active) {
    this.active = active;
  }

  public IntrospectionResponse(String error) {
    this.error = error;
  }

  public boolean isActive() {
    return active;
  }

}
