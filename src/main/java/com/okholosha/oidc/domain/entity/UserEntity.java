package com.okholosha.oidc.domain.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Getter
@Setter
@ToString(callSuper = true)
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class UserEntity extends ResourceEntity {

    @Column(unique = true)
    @NotBlank
    @Size(min = 3, max = 50)
    private String userName;
    @NotBlank
    @Size(min = 1, max = 100)
    private String familyName;
    @NotBlank
    @Size(min = 1, max = 100)
    private String givenName;
    @Size(max = 100)
    private String userType;
    @Size(max = 50)
    private String locale;
    @NotNull
    private boolean active;
    @NotBlank
    @Size(min = 8, max = 255)
    private String password;
    private String email;
    @ElementCollection
    private Set<String> roles = new HashSet<>();

    public UserEntity(UUID identifier, String userName, String familyName, String givenName, boolean active,
                      String password, String email, Set<String> roles) {
        this(identifier, identifier.toString(), userName, familyName, givenName,
                active, password, email, roles);
    }

    public UserEntity(UUID identifier, String externalId, String userName, String familyName, String givenName,
                      boolean active, String password, String email, Set<String> roles) {
        this(identifier, externalId, userName, familyName, givenName, null, null,
                active, password, email, roles);
    }

    public UserEntity(UUID identifier, String externalId, String userName, String familyName,
                      String givenName, String userType, String locale, boolean active,
                      String password, String email, Set<String> roles) {
        super(identifier, externalId);
        this.userName = userName;
        this.familyName = familyName;
        this.givenName = givenName;
        this.userType = userType;
        this.locale = locale;
        this.active = active;
        this.password = password;
        this.email = email;
        this.roles = roles;
    }

    public boolean isActive() {
        return active;
    }
}
