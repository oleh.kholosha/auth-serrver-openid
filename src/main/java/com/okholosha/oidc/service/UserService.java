package com.okholosha.oidc.service;

import com.okholosha.oidc.domain.dto.UserResource;
import com.okholosha.oidc.domain.entity.UserEntity;
import com.okholosha.oidc.repository.ScimUserEntityRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.IdGenerator;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional(readOnly = true)
@AllArgsConstructor
public class UserService {

    private final ScimUserEntityRepository scimUserEntityRepository;
    private final IdGenerator idGenerator;

    public Optional<UserEntity> findUserByIdentifier(UUID identifier) {
        return scimUserEntityRepository.findOneByIdentifier(identifier);
    }

    public Optional<UserEntity> findUserByUserName(String username) {
        return scimUserEntityRepository.findOneByUserName(username);
    }

    @PreAuthorize("hasRole('ADMIN')")
    public List<UserEntity> findAllUsers() {
        return scimUserEntityRepository.findAll();
    }

    @PreAuthorize("hasRole('ADMIN')")
    @Transactional
    public UserEntity createUser(UserEntity scimUserEntity) {
        if (scimUserEntity.getIdentifier() == null) {
            scimUserEntity.setIdentifier(idGenerator.generateId());
        }
        return scimUserEntityRepository.save(scimUserEntity);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @Transactional
    public UserEntity updateUser(UUID userIdentifier, UserResource scimUserResource) {
        return scimUserEntityRepository.findOneByIdentifier(userIdentifier)
                .map(ue -> {
                    ue.setActive(scimUserResource.isActive());
                    ue.setUserName(scimUserResource.getUserName());
                    ue.setFamilyName(scimUserResource.getFamilyName());
                    ue.setGivenName(scimUserResource.getGivenName());
                    ue.setLocale(scimUserResource.getLocale());
                    ue.setExternalId(scimUserResource.getExternalId());
                    ue.setRoles(scimUserResource.getRoles());
                    ue.setUserType(scimUserResource.getUserType());
                    return scimUserEntityRepository.save(ue);
                }).orElseThrow(() -> new UserNotFoundException(userIdentifier));
    }

    @PreAuthorize("hasRole('ADMIN')")
    @Transactional
    public void deleteUser(UUID userIdentifier) {
        scimUserEntityRepository.deleteOneByIdentifier(userIdentifier);
    }
}
